**Is a system call just a function call? Why or why not?** 
<br>
Conceptually, a system call is like a function call but a system call has another layer of security requirements to satisfy. 

**Why do system calls need to exist?**
<br>
To allow processes to interact with operating systems, we need extra security to prevent the processes from avoiding the operating system’s control.

**Explain the registers that are used to interface to the system call.** 
<br>
ssize_t write(int fd, const void *buf, size_t count); <br>
The system call uses the write call above. R0 (int fd) holds 1 which is the number of file descriptor. R1(const void *buf) is used to hold the data of the greeting string to write. R2(size_t count) holds the size of the greeting string to write. The number 4 is for call ‘write’ and r7 holds that number 4 so it can perform the system call ‘write’.
